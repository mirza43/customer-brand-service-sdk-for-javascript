import CustomerBrandSynopsisView from './customerBrandSynopsisView';
import Level2CustomerBrandView from './level2CustomerBrandView';

export default class Level2CustomerBrandViewFactory {

    static construct(data):Level2CustomerBrandView {

        const level1CustomerBrand =
            new CustomerBrandSynopsisView(
                data.level1CustomerBrand.id,
                data.level1CustomerBrand.name
            );

        return new Level2CustomerBrandView(
            data.id,
            data.name,
            data.customerSegmentId,
            level1CustomerBrand
        );

    }

}