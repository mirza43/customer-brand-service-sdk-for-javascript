import CustomerBrandView from './customerBrandView';
import CustomerBrandSynopsisView from './customerBrandSynopsisView';

/**
 * @class {Level2CustomerBrandView}
 */
export default class Level2CustomerBrandView extends CustomerBrandView {

    _level1CustomerBrand:CustomerBrandSynopsisView;

    constructor(id:number,
                name:string,
                customerSegmentId:number,
                level1CustomerBrand:CustomerBrandSynopsisView) {

        super(id, name, customerSegmentId);

        if (!level1CustomerBrand) {
            throw new TypeError('level1CustomerBrand required');
        }
        this._level1CustomerBrand = level1CustomerBrand;

    }

    /**
     *
     * @returns {CustomerBrandSynopsisView}
     */
    get level1CustomerBrand():CustomerBrandSynopsisView {
        return this._level1CustomerBrand;
    }
}
